# README #

### What is Phishing Boat Email? ###

Phishing Boat Email is a Google Chrome extension that offers protection
against unfamiliar emails.

### How does it work? ###

The basic idea is that, if one receives an email from an unfamiliar email
address, then Phishing Boat Email will inform the user that the email is
unfamiliar and tell him/her to either confirm that the email is legitimate
or avoid acting on any information in the email until its legitimacy can be
verified.

### How does that help stop phishing attacks? ###

Skilled hackers will craft phishing emails that appear to be from legitimate
websites but are actually from phony email addresses, such as
"jane.smith@gmal.com" instead of "jane.smith@gmail.com". This can fool users
into acting on instructions or downloading attachments that appeared to come
from a legitimate source but actually came from a hacker. Phishing Boat Email
uses one's cookies to determine which email addresses one has received email
from recently so that one doesn't confuse a convincing but illegitimate email
for a legitimate email.

### How do I get set up? ###

Go to (insert google chrome extensions store url here) and click
install. Currently, this extension is available only for Google
Chrome. Support for other browsers may be added as schedules
permit.

### Contribution guidelines ###

If submitting a change, please submit a pull request that briefly
explains your change. If submitting an issue, then please
describe which website you attempted to visit (the URL of the link
you clicked on will be fine) and which webpage you currently were
on (the URL in your browser bar will be fine). Without this, we
usually cannot address your issue fully. If possible, provide
information on any webpage you had previously visited, the date
and time when you experienced this issue, and which operating system
you were using.

### Who do I talk to? ###

Andrew Zuelsdorf is the repository manager and the creator of this
software. You can reach him at andrew.z1@yandex.com.
