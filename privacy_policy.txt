Q: What information does Phishing Boat Email share?

A: All information used by Phishing Boat Email is stored on your device.
It is not shared with anyone for any reason.

Q: What information does Phishing Boat Email collect?

A: The information that Phishing Boat collects is limited to the time that you opened Gmail for the first time after installing the extension plus the sender email addresses from emails you have received in Gmail after installing the extension. 

Q: How does Phishing Boat Email use this information?

A: This information is solely used for Phishing Boat Email to tell you whether you should be warned about an email or not (in particular, it is used to determine whether you have received an email from the given sender before and whether the extension is still in its learning phase). It is not used for any other purposes.

