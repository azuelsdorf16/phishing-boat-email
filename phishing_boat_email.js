// Copyright (c) 2018 Andrew Zuelsdorf. All rights reserved.

var cookies = {};

var debug = false;

var isChrome = false;

var checked_email_urls = [];

var learningPeriodTimeDays = 14;

var learningPeriod = 86400 * learningPeriodTimeDays * 1000;

//Updates our cookie hashtable and our Chrome local storage
//to indicate that we have visited a webpage
function setCookie(cookieName, value) {
    var query = {};

    query["" + cookieName] = value;

    cookies["" + cookieName] = value;

    if (isChrome) {
        chrome.storage.local.set(query, function() {});
    } else {
        browser.storage.local.set(query);
    }
}

//Get time (seconds) remaining in a learning period.
function timeInLearningPeriod() {
    var installTime = getCookieValue("install_time");

    return (learningPeriod - (Date.now() - installTime)) / 1000.0;
}

//Test whether we are in a learning period or not.
function inLearningPeriod() {
    var timeLeft = timeInLearningPeriod();

    if (timeLeft > 0) {
        return true;
    } else {
        return false;
    }
}

//Check whether we have received an email from this email address or not recently.
function cookieExists(cookieName) {
    return getCookieValue(cookieName) != null;
}

//Sets a cookie if it does not already exist.
function setCookieIfNotExists(cookieName, value) {
    if (!cookieExists(cookieName)) {
        setCookie(cookieName, value);
    }
}

//Get value of cookie, if any.
function getCookieValue(cookieName) {
    if (debug) {
        var cookieString = "[ ";

        for (var c in cookies) {
            cookieString += "\"" + c + "\" ";
        }

        cookieString += " ]";
    }

    return cookies[cookieName];
}

//Confirm whether email is new but trusted, new and untrusted, old and
//untrusted, or old and trusted.
function confirm_email(email_addr) {
    var response = true;

    var message = "You have not received emails from \"" + email_addr +
        "\" recently." +
        " If you trust this email address, then click \"OK\". Otherwise, click " +
        "\"Cancel\" and ignore all information, including attachments, in the " +
        "email until you know exactly who sent it.";

    //If cookie does not exist and if we have not already checked the email at this
    //URL and if we are not in a learning period, then prompt the user to confirm or reject the email.
    if (!inLearningPeriod() && checked_email_urls.indexOf(window.location.href) === -1 &&
        !cookieExists(email_addr)) {

        response = window.confirm(message);

        //Make note that we checked this email already so that, if the user has
        //already told us that it doesn't recognize the sender, then we won't
        //repeatedly query the user on the next periodics. Basically, we don't
        //want to keep asking a question that the user already answered
        //sufficiently.
        checked_email_urls.push(window.location.href);
    }

    if (response) {
        setCookie(email_addr, 1);
    }
}

//Finds sender email address on Gmail webpage, if one exists (one won't exist
//on the page that shows all emails). If sender email exists, will evaluate
//whether email is new and ask user to confirm or whether email is old and
//not say anything.
function scan_for_emails() {
    // Make sure we are reasonably certain that we are scanning a real email.
    var folder_regexes = [
        /https\:\/\/mail\.google\.com\/mail\/u\/[0-9]+\/\#inbox\/.+/gm,
        /https\:\/\/mail\.google\.com\/mail\/u\/[0-9]+\/\#starred\/.*/gm,
        /https\:\/\/mail\.google\.com\/mail\/u\/[0-9]+\/\#snoozed\/.*/gm,
        /https\:\/\/mail\.google\.com\/mail\/u\/[0-9]+\/\#imp\/.*/gm,
        /https\:\/\/mail\.google\.com\/mail\/u\/[0-9]+\/\#sent\/.*/gm,
        /https\:\/\/mail\.google\.com\/mail\/u\/[0-9]+\/\#category\/[^\/]+\/.*/gm,
        /https\:\/\/mail\.google\.com\/mail\/u\/[0-9]+\/\#label\/.*/gm,
        /https\:\/\/mail\.google\.com\/mail\/u\/[0-9]+\/\#all\/.*/gm,
        /https\:\/\/mail\.google\.com\/mail\/u\/[0-9]+\/\#spam\/.*/gm,
        /https\:\/\/mail\.google\.com\/mail\/u\/[0-9]+\/\#trash\/.*/gm,
        /https\:\/\/mail\.google\.com\/mail\/u\/[0-9]+\/\#search\/[^\/]+\/.*/gm
    ];

    var is_folder_url = false;

    for (var i = 0; i < folder_regexes.length; ++i) {
        if (window.location.href.match(folder_regexes[i]) != null) {
            is_folder_url = true;
        }
    }

    if (!is_folder_url) {
        return;
    }

    var email_url_regex = /\/[a-z]+$/gi;

    var is_email = email_url_regex.exec(window.location.href);
    
    if (is_email === null || is_email.length == 0) {
        return;
    }

    var tables = document.getElementsByTagName("table");

    if (tables == null || tables.length == 0) {
        return;
    }

    var sender_email_addr = null;

    for (var i = 0; i < tables.length; ++i) {
        var email_table = tables[i];
        var email_table_role = email_table.getAttribute("role");

        if (email_table_role != null && email_table_role == "presentation") {
            var regexpr = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/gi;
            //Regex taken from
            //https://stackoverflow.com/questions/46155/how-to-validate-an-email-address-in-javascript
            //Warnings from https://www.regular-expressions.info/email.html
            //read and understood.

            var matches = regexpr.exec(email_table.innerHTML);

            for (var j = 0; matches != null && j < matches.length; ++j) {
                var match_j = matches[j];

                if (match_j != null && match_j.trim() != "") {
                    sender_email_addr = match_j;
                    break;
                }
            }
        }

        if (sender_email_addr != null) {
            break;
        }
    }

    if (sender_email_addr != null) {
        confirm_email(sender_email_addr);
    } else {
        alert("WARNING: Phishing Boat Email couldn't identify this email's sender. Please manually verify the email's sender before acting on the information in the email.");
    }
}

//Retrieves every item in our local storage and adds it to our "cookies"
//object (dictionary/hashtable). This way, we will have a list of our
//previously-visited websites that we can reference synchronously.
function loadCookies() {
    //Get "items", an object that contains each item in our local storage.
    var callback = function (items) {
        for (var key in items) {
            var tempKey = "" + key; //Convert item to a string so that we don't try
            //to call undefined methods on an object.

            cookies[tempKey] = items[key]; //Store the item.
        }

        setCookieIfNotExists("install_time", Date.now());
   
        if (debug) {
            var timeLeft = timeInLearningPeriod();

            alert(timeLeft + " seconds left in learning period.");
        }
    };

    if (isChrome) {
        chrome.storage.local.get(null, callback);
    } else {
        getPromise = browser.storage.local.get(null);
        getPromise.then(callback);
    }
}

function main() { 
    //Load history of visited sites from Chrome's local storage. This way, we
    //know which sites we have visited and we can compare domain names in links
    //against pages we have visited.
    loadCookies();

    //Create a periodic that will scan for sender emails on the current webpage
    
    var handler = function () { scan_for_emails(); };

    window.setInterval(handler, 800);
}

main();
